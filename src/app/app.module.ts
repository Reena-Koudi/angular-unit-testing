import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { TakeAPicComponent } from './components/take-a-pic/take-a-pic.component';
import { TryOnComponent } from './components/try-on/try-on.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TakeAPicComponent,
    TryOnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
