import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/components/home/home.component';
import { TakeAPicComponent } from 'src/app/components/take-a-pic/take-a-pic.component';
import { TryOnComponent } from 'src/app/components/try-on/try-on.component';

export const routes: Routes = [
  {path: '' , component: HomeComponent },
  {path: 'tryon' , component: TryOnComponent },
  {path: 'takeapic' , component: TakeAPicComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export const routingComponents = [HomeComponent, TryOnComponent, TakeAPicComponent];
